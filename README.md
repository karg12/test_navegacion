# Prueba de conocimientos

_Esta es una prueba de conocimientos que muestra un filtro de busqueda con las gasolineras por Estado, Municipio o su precio el cual arroja un mapa_

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._


### Pre-requisitos 📋

_Que cosas necesitas para instalar el software y como instalarlas_

```
PHP en la versión 7.3.31 o superior
```

### Instalación 🔧

_Instrucciones_

_1) Arrancar el servidor local Apache con MySQL_
_2) Crear la Base de datos "gasolineras" en MySQL_
_2) Clonar el proyecto en la carpeta correspondiente al servidor para Xampp en la carpeta htdocs o para Wamp en la carpeta www_

```
git clone https://bitbucket.org/karg12/test_navegacion.git
```

_4) En la terminal dentro del proyecto "test_navegacion" ejecutar las líneas:_

```
composer install
php artisan migrate
```
_5) Importar la Base de Datos que se localiza en el proyecto "test_navegacion/public/gasolineras.sql" en el motor de la base de datos MySQL_
_6) Ejecutar la línea_
```
php artisan serv 
```
_Ingresar a _
```
http://127.0.0.1:8000
```

_ó_
```
http://localhost/test_navegacion/public/
```

_7) Ir al menú superior en Buscar para enlazar al formulario._


## Construido con


* [Laravel 8](https://laravel.com) - El framework web usado


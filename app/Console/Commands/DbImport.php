<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File; 

class DbImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importa la base de datos gasolineras.sql';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        ini_set('memory_limit', '-1');
        $file =public_path('gasolineras.sql');
        // // DB::unprepared(file_get_contents ($file));

        // DB::unprepared(File::get($file));
        // // DB::unprepared(File::get($file));
        // $this->line('<fg=red;bg=yellow>Comando para Importar Base de Datos db:import</>');


        // \Artisan::call('migrate:reset', ['--force' => true]);
        // DB::unprepared(file_get_contents($file));
        // \Artisan::call('migrate');

        $sql_dump = File::get($file);
        DB::connection()->getPdo()->exec($sql_dump);

    }
}

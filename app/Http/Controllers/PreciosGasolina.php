<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CodPostales as CP;//Importar el modelo de Codigo Postales
use Illuminate\Support\Facades\Http; //importar cliente HTTP

class PreciosGasolina extends Controller
{
    public function show_precios(Request $req){

        $response = Http::get('https://api.datos.gob.mx/v1/precio.gasolina.publico');//Consumir la Api de los precios de gasolina
        $precios = $response->json();//Convertir los datos obtenidos a JSON para su lectura
        
        /*
            Inicialisamos las varibles que llevaran el dato filtro
        */
        $c_estado = $req->input('estado');
        $c_mnpio = $req->input('municipio');
        $order = $req->input('order');
    
            /*
                Se realiza la consulta para obtener los datos por estado y/o municipio
            */
            if(!empty($c_mnpio)){
                $CodigosPostales = CP::select('d_estado','d_codigo','D_mnpio')->Where([
                    ['c_estado',$c_estado],
                    ['c_mnpio',$c_mnpio]
                ])->groupBy('d_codigo','d_estado','D_mnpio')->get()->toArray();
            }else{
                $CodigosPostales = CP::select('d_estado','d_codigo','D_mnpio')->Where([
                    ['c_estado',$c_estado]
                ])->groupBy('d_codigo','d_estado','D_mnpio')->get()->toArray();
            }


            $array1 = array();
            $array2 = array();
            /*
                Recorremos los dos arreglos (codigos postales y precios)
            */
            $preciosArray = $precios['results'];//Se toma el Arreglo "results" de nuestro arreglo Precios
            foreach($preciosArray as $precio){
                foreach($CodigosPostales as $codigo){
                    if( $precio['codigopostal'] === $codigo['d_codigo']){
                        $array1['id'] = $precio['_id'];
                        $array1['rfc'] = $precio['rfc'];
                        $array1['razonsocial'] = $precio['razonsocial'];
                        $array1['date_insert'] = $precio['date_insert'];
                        $array1['numeropermiso'] = $precio['numeropermiso'];
                        $array1['fechaaplicacion'] = $precio['fechaaplicacion'];
                        $array1['ufeffpermisoid'] = empty($precio['permisoid'])?"":$precio['permisoid'];
                        $array1['longitude'] = $precio['longitude'];
                        $array1['latitude'] = $precio['latitude'];
                        $array1['codigopostal'] = $precio['codigopostal'];
                        $array1['calle'] = $precio['calle'];
                        $array1['colonia'] = $precio['colonia'];
                        $array1['municipio'] = $codigo['D_mnpio'];
                        $array1['estado'] = $codigo['d_estado'];
                        $array1['regular'] = $precio['regular'];
                        $array1['premium'] = $precio['premium'];
                        $array1['dieasel'] = $precio['dieasel'];

                        array_push($array2,$array1);
                    }
                }
            }

            if(!empty($array2)){
                if(!empty($order)){
                    $conPrecios = $this->searchByValue($order,$array2);//Se llama la funcion para traer los valores de acuerdo al ordenamiento
                    if(!empty($conPrecios)){
                        $result = json_encode(array(
                            'success'=>true,
                            'results'=>$conPrecios
                        ));
                    }else{
                        $result = json_encode(array(
                            'success'=>false,
                            'results'=>"No se encontraron resultados para ese filtro de busqueda 1"
                        ));                        
                    }
                }else{
                    $result = json_encode(array(
                        'success'=>true,
                        'results'=>$array2
                    ));
                }
            }else{
                $result = json_encode(array(
                    'success'=>false,
                    'results'=>"No se encontraron resultados para ese filtro de busqueda 2"
                ));
            }
        return $result;
    }

    /*
        Funcion para traer los las gasolineras con forme a los precios capturados
    */
    function searchByValue($id, $array1) {
        foreach ($array1 as $key => $val) {
            if ($val['regular'] == $id || $val['premium'] == $id || $val['dieasel'] == $id ) {
                $array1[$key]['id'] = $val['id'];
                $array1[$key]['rfc'] = $val['rfc'];
                $array1[$key]['razonsocial'] = $val['razonsocial'];
                $array1[$key]['date_insert'] = $val['date_insert'];
                $array1[$key]['numeropermiso'] = $val['numeropermiso'];
                $array1[$key]['fechaaplicacion'] = $val['fechaaplicacion'];
                $array1[$key]['ufeffpermisoid'] = empty($val['ufeffpermisoid'])?"":$val['ufeffpermisoid'];
                $array1[$key]['longitude'] = $val['longitude'];
                $array1[$key]['latitude'] = $val['latitude'];
                $array1[$key]['codigopostal'] = $val['codigopostal'];
                $array1[$key]['calle'] = $val['calle'];
                $array1[$key]['colonia'] = $val['colonia'];
                $array1[$key]['municipio'] = $val['municipio'];
                $array1[$key]['estado'] = $val['estado'];
                $array1[$key]['regular'] = $val['regular'];
                $array1[$key]['premium'] = $val['premium'];
                $array1[$key]['dieasel'] = $val['dieasel'];
                return $array1;
            }
        }
        return null;
    }
}
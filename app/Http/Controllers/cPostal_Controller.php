<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CodPostales as CP;//Modelo de la tabla codpostal

class cPostal_Controller extends Controller
{
    public function form_cp(){
        /*
          Se realiza una consulta para traer los estados sin repetirse
        */
        $estados = CP::select('c_estado','d_estado')->where('d_estado','<>','')->groupBy('c_estado','d_estado')->get();
        return view('Buscar.form_buscar',compact('estados'));
    }

    public function emp(Request $req){
      $edo = $req->get('id');
      /*
        Se realiza la consulta para traer los municipios conforme al ID del Estado
      */
		  $datos = CP::select('D_mnpio','c_mnpio')->where('c_estado', $edo)->groupBy('D_mnpio','c_mnpio')->get();
		  return $datos;
    }

}

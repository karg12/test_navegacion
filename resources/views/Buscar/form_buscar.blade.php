@extends('adminlte::page')
@section('title', 'Buscar')
@section('content_header')
    <h1>Incio</h1>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="" />
    <style>		
        #map {
			width: 600px;
			height: 400px;
		}
	</style>
@stop
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <form onsubmit="return false" method="POST"  id="formulario" name="formulario"  data-toggle="validator" role="form" >
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <x-adminlte-select name="estado" id="estado" igroup-size="lg"  required>
                                    <option value="" selected disabled>--Selecciona Estado</option>
                                    @foreach($estados as $estado)
                                        <option value="{{$estado->c_estado}}">{{$estado->d_estado}}</option>
                                    @endforeach
                                </x-adminlte-select>
                            </div>                            
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <x-adminlte-select name="municipio" id="municipio" igroup-size="lg">
                                    <option value="" selected disabled>--Selecciona Municipio--</option>
                                </x-adminlte-select>
                            </div>                            
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <x-adminlte-input  type="number" step="0.01" name="order" id="order" placeholder="Escribe una cantidad"
                                igroup-size="lg" disable-feedback/>
                            </div>                            
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <button type="submit" class="btn btn-success">
                            Buscar
                        </button>                    
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <div id="map"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="showTable" style="display:none;">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body" >
                    <div class="table-responsive">
                        <table class="table table-bordered" id="tabladatos">
                            <thead>
                                <th>CP</th>
                                <th>Estado</th>
                                <th>Municipio</th>
                                <th>Colonia</th>
                                <th>Calle</th>
                                <th>Razon Social</th>
                                <th>RFC</th>
                                <th>Longitud</th>
                                <th>latitud</th>
                                <th>Premium</th>
                                <th>Regular</th>
                                <th>Dieasel</th>
                            </thead>
                            <tbody id="resul"> </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('css')

@stop

@section('js')
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
    <script> 
        var map = null;
        $("#estado").change(function () {
            var edo = document.getElementById("estado").value;
            $.ajax({
                method: 'GET', 
                url: "{{route('buscaredo')}}", 
                data: {'id': edo}, 
                success: function(data){ 
                    // console.log(data); 
                    var sub = "<option value='' selected disabled>--Selecciona Municipio--</option>";
                    for (var i = 0; i < data.length; i++) sub += '<option value="' + data[i].c_mnpio + '">' + data[i].D_mnpio + "</option>";
                    $("#municipio").html(sub);
                },
                error: function(jqXHR, textStatus, errorThrown) { // Si hay un error lo muestra en la consola
                    console.log(JSON.stringify(jqXHR));
                    console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                }
            });
        });


        $('#formulario').on('submit', function () {
            console.log(map); 
            if (map !== undefined && map !== null) {
                map.remove(); 
            }
            var estado = $("select[name='estado']",this).val();
            var municipio = $("select[name='municipio']",this).val();
            var order = $("input[name='order']",this).val();

            $.ajax({
                url: "{{route('buscarPrecios')}}",
                type: "Post",
                data: JSON.stringify({
                    "_token": "{{ csrf_token() }}",
                    estado: estado,
                    municipio: municipio,
                    order: order,
                }),
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    var arrayResponse = JSON.parse(data);
                    console.log(arrayResponse);

                    if(arrayResponse.success == false){
                        $('#showTable').css('display', 'none');
                        map = null;
                        Swal.fire({
                            icon: 'warning',
                            title: 'Oops...',
                            text: arrayResponse.results
                        })
                    }else{
                        var arrayDatos = arrayResponse.results;
                        if(arrayDatos.length>0 ){
                            $('#showTable').css('display', 'block');

                            load_map_and_analyze_data(arrayDatos);

                            $('#tabladatos').DataTable().clear().destroy();
                            var table = $('#tabladatos').DataTable({
                                processing: true,
                                aaData:arrayDatos,
                                aoColumns:[
                                    {"data": "codigopostal"},
                                    {"data": "estado"},
                                    {"data": "municipio"},
                                    {"data": "colonia"},
                                    {"data": "calle"},
                                    {"data": "razonsocial"},
                                    {"data": "rfc"},
                                    {"data": "longitude"},
                                    {"data": "latitude"},
                                    {"data": "premium"},
                                    {"data": "regular"},
                                    {"data": "dieasel"},                                    
                                ],
                                
                                "language": {
                                    "emptyTable": "No hay información",
                                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                                    "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                                    "infoPostFix": "",
                                    "thousands": ",",
                                    "lengthMenu": "Mostrar _MENU_ Entradas",
                                    "loadingRecords": "Cargando...",
                                    "processing": "Procesando...",
                                    "search": "Buscar:",
                                    "zeroRecords": "Sin resultados encontrados",
                                    "paginate": {
                                        "first": "Primero",
                                        "last": "Ultimo",
                                        "next": "Siguiente",
                                        "previous": "Anterior"
                                    }
                                },
                                destroy: true
                            });
                        }else{
                            $('#showTable').css('display', 'none');
                            map = null;
                            Swal.fire({
                                icon: 'warning',
                                title: 'Oops...',
                                text: "Lo sentimos no se encontraron resultados"
                            })
                        }
                    }
                },
                failure: function (data){
                    alert(data.responseText);
                },
                error: function (data){
                    alert(data.responseText);
                }
            });
        })

        function load_map_and_analyze_data(arrayDatos) {
                                        // if(map==null) map=new L.Map('idopenstreet').setView();
            var cities = L.layerGroup();
            for (i = 0; i < arrayDatos.length; i++) {
                marker = L.marker([arrayDatos[i]['latitude'], arrayDatos[i]['longitude']]).bindPopup( arrayDatos[i]['razonsocial']);
                cities.addLayer(marker);
            }
            var mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, ' +
                    'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                mbUrl = 'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw';

            var grayscale   = L.tileLayer(mbUrl, {id: 'mapbox/light-v9', tileSize: 512, zoomOffset: -1, attribution: mbAttr}),
                streets  = L.tileLayer(mbUrl, {id: 'mapbox/streets-v11', tileSize: 512, zoomOffset: -1, attribution: mbAttr});
            

            map = L.map('map', {
                center: [19.4326, -99.13],
                zoom: 4,
                layers: [grayscale, cities]
            });

            var baseLayers = {
                "Grayscale": grayscale,
                "Streets": streets
            };

            var overlays = {
                "Cities": cities
            };


            L.control.layers(baseLayers, overlays).addTo(map);
        }

    </script>


@stop